import pynvim 
import re
import os

@pynvim.plugin
class viTex(object):
    vim=None
    projectFile=None
    def __init__(self, vim):
        self.vim = vim
        # self.cf=vim.current.buffer
        # self.cw=vim.current.window

    @pynvim.command('LtCompileFile', sync=True, nargs=0)
    def compileFile(self):
        if  self.projectFile  is None: 
            self.vim.out_write('No source file set. Current file will be used')
            srcFile = self.vim.eval('expand("%")')
        else:
            srcFile = self.projectFile

        command = 'pdflatex ' + srcFile
        self.vim.command('echo "{}"'.format(command))
        os.system(command)

    @pynvim.command('LtSetSourceFile', sync=True, nargs='*')
    def setSourceCode(self, args):
        if args:
            srcFile = self.vim.eval(args[0])
        else:
            srcFile = self.vim.eval('expand("%")')

        self.projectFile = srcFile
        self.vim.out_write(self.projectFile)

    @pynvim.command('LtStartDoc', sync=True, nargs=0)
    def startDoc(self):
        vim = self.vim
        cf = self.vim.current.buffer
        text = '\\documentclass{}\n\\usepackage{}\n\\begin{document}\n\\end{document}'
        cf[:] = text.split('\n')

    @pynvim.command('LtEnv', sync=True, nargs=0)
    def beginEnd(self):
        vim = self.vim
        cf = vim.current.buffer
        cw = vim.current.window
        # cf=self.cf
        # cw=self.cw
        row, col = cw.cursor
        r = row -1
        name = vim.eval('expand("<cword>")')
        extra = None
        newline = ''
        if name == '[':
            cf[r] = '\t\\['
            cf.append('\t\\]', r + 1)
        else:
            if 'array' in name:
                extra = name.split('array')[-1]
                name = 'array'
                if len(extra)>0:
                    newline = (len(extra) - 1) * ' &' + ' \\\\'
            elif 'frame' in name:
                extra = name.split('frame')[-1]
                name = 'frame'
                newline = ''
            elif name in ['enumerate', 'itemize', 'description']:
                newline = '\\item '

            cf[r] = name.join(['\\begin{', '}']) + ('' if extra is None else  extra.join(['{', '}']))
            cf.append(name.join(['\\end{', '}']), r + 1)
            cf.append(newline, r + 1)
            

    @pynvim.command('LtLR', sync=True, nargs=0)
    def leftRight(self):
        cf = self.vim.current.buffer
        cw = self.vim.current.window
        row, col = cw.cursor
        r = row -1
        name = self.vim.eval('expand("<cword>")')
        if name == '[':
            right = ']'
        elif name == '(':
            right = ')'
        elif name == '{':
            name = '\\{'
            right = '\\}'
        elif name == '|':
            name = '\\|'
            right = '\\|'
        name = '\\left' + name
        right = '\\right' + right
        cf[r] = name
        cf.append( right, r + 1)


    def getFormula(self):
        cf = self.vim.current.buffer
        cw = self.vim.current.window
        row, col = cw.cursor
        col +=1
        cl = self.vim.current.line
        left = cl[:col]
        right = cl[col:]
        if ('$' in left) or ('\[' in left):
            if '$' in left:
                left = left.split('$')[-1]
                if '\[' in left:
                    left = left.split('\[')[-1] + right.split('\]')[0]
                else:
                    left += right.split('$')[0]
            else:
                left = left.split('\[')[-1] + right.split('\]')[0]
        else:
            left += right
        return left

    @pynvim.command('LtFormulaToReg', sync=True, nargs=0)
    def formulaToReg(self):
        ff = self.getFormula()
        ff = ff.replace('\\','\\\\')
        aux = 'let @\" =\"' + ff + '\"'
        self.vim.command(aux)

    @pynvim.command('LtFormulaToFile', sync=True, nargs=0)
    def formulaToFile(self):
        ff = self.getFormula() 
        f = open('f.tex', 'a')
        f.write(ff + '\n')
        f.close()

        ff = ff.replace('\\','\\\\')
        aux = 'let @\" =\"' + ff + '\"'
        self.vim.command(aux)

    @pynvim.command('LtMinProg', sync=True, nargs=0)
    def minProg(self):
        cf = self.vim.current.buffer
        cw = self.vim.current.window
        row, col = cw.cursor
        r = row -1
        aux = '\\begin{array}{rl}\n\\min & f\\\\\n\\text{s.t.} & \\\\\n\\end{array}'
        cf.append(aux.split('\n'),r)


    def findEnvStart(self, line, val):
        aux = re.finditer(r'\\begin\{\w+\}|\\end\{\w+\}', line)
        if aux is  None:
            return line, val
        else: 
            Lee = list(aux)
            while ((val <=0) and (len(Lee)>0)):
                ee = Lee[-1]
                Lee = Lee[:-1]
                i,j = ee.span()
                if 'end' in line[i:j]:
                    val -= 1
                else:
                    val += 1
                if val > 0:
                    return line[i:], val

            return line, val

    def findEnvEnd(self, line, val):
        aux = re.finditer(r'\\begin\{\w+\}|\\end\{\w+\}', line)
        if aux is  None:
            return line, val
        else: 
            Lee = list(aux)
            while ((val >0) and (len(Lee)>0)):
                ee = Lee[0]
                Lee = Lee[1:]
                i,j = ee.span()
                if 'begin' in line[i:j]:
                    val += 1
                else:
                    val -= 1
                if val == 0:
                    return line[:j], val
            return line, val

    @pynvim.command('LtCopyEnv', sync=True, nargs=0)
    def copyEnv(self):
        cf = self.vim.current.buffer
        cw = self.vim.current.window
        row, col = cw.cursor
        val = 0
        rowAux = row -1
        line = self.vim.current.line
        lineAux = line[: col+1]
        lines = []

        while (val <= 0):
            lineAux, val = self.findEnvStart(lineAux, val)
            lines = [lineAux] + lines
            rowAux -= 1
            lineAux = cf[rowAux]

        text = '\n'.join(lines)

        rowAux = row-1
        line = self.vim.current.line
        lineAux = line[col:]
        lines = []
        while (val >0):
            lineAux, val = self.findEnvEnd(lineAux, val)
            lines += [lineAux]
            rowAux += 1
            lineAux = cf[rowAux]
        text = text + '\n'.join(lines)

        text = text.replace('\\','\\\\')
        aux = 'let @\" =\"' + text + '\"'
        self.vim.command(aux)

        # self.vim.out_write(text)

    @pynvim.command('LtHelp', sync=True, nargs=0)
    def texHelp(self):
        message = 'viTex Help\n==========\n'
        message += '\n'.join(['StartDoc', 'BeginEnd', 'MinProg'])
        self.vim.out_write(message)
